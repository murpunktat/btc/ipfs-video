# ipfs-video

utilize IPFS for adaptive video transmission

## motivation:

handle video file hosting as efficent and satisfing as provided by
the global players (i.e. adaptive streaming and CDN replication), 
but keep the freedom of self hosting, image quality control etc.

## realization:

- utilize gitlabs CI system (i.e. users don't have to maintan unix machines and uncommon software setups)
- utilize GIT LFS (large file support) for the actual video data in the repository
- prepare adaptive HLS streaming by spliting the files with [`video2hls`](https://github.com/vincentbernat/video2hls)
- add the [local gitlab page content](https://murpunktat.gitlab.io/btc/ipfs-video/index.html) to IPFS
- the video is now accessable via the [common static webpage](https://murpunktat.gitlab.io/btc/ipfs-video/video_test.html) or over any [IPFS gateway](https://murpunktat.gitlab.io/btc/ipfs-video/video_ipfs_test.html)

## troubles:

- Git LFS usage is rather complicated
- HLS preparation via gitlab CI is horrible slow
- the transfer to IPFS resp. pinning needs a local daemon running as background job
- ...